package com.sample;

public abstract class Notebook extends Book {
 
	public void draw() {
		System.out.println("draw a picter");
	}
	public void write() {
		System.out.println("write a book");
	}
	public void read() {
		System.out.println("read a book");
	}
	public static void main(String[] args) {
		Notebook note=new  Notebook();
		note.draw();
		note.write();
		note.read();
		
	}
}
